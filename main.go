package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"gitlab.com/faizalnurrozi/go-gin-repository/src"
)

func main() {
	r := gin.Default()

	/**
	 * Register all route
	 */

	groupRouting := src.NewRouterGroup(r)
	groupRouting.Register()

	/**
	 * Run application
	 */

	err := r.Run()
	if err != nil {
		fmt.Println(err)
	}
}
