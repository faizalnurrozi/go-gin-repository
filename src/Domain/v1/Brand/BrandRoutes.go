package Brand

import (
	"github.com/gin-gonic/gin"
)

type Route struct {
	routerGroup *gin.RouterGroup
}

func NewRoute(rg *gin.RouterGroup) *Route {
	return &Route{
		routerGroup: rg,
	}
}

func (route Route) Register() {

	collections := route.routerGroup.Group("brand")
	{
		collections.GET("/", func(context *gin.Context) {
			context.JSON(200, gin.H{
				"message": "Brand list",
			})
		})

		collections.GET("/:id", func(context *gin.Context) {
			context.JSON(200, gin.H{
				"message": "Brand detail " + context.Param("id"),
			})
		})

		collections.POST("/", func(context *gin.Context) {
			context.JSON(200, gin.H{
				"message": "Brand add",
			})
		})

		collections.PUT("/:id", func(context *gin.Context) {
			context.JSON(200, gin.H{
				"message": "Brand edit " + context.Param("id"),
			})
		})

		collections.DELETE("/:id", func(context *gin.Context) {
			context.JSON(200, gin.H{
				"message": "Brand delete " + context.Param("id"),
			})
		})
	}
}