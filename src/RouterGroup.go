package src

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/faizalnurrozi/go-gin-repository/src/Domain/v1/Brand"
)

type GroupRouting struct {
	Engine *gin.Engine
}

func NewRouterGroup(routing *gin.Engine) *GroupRouting {
	return &GroupRouting{
		Engine: routing,
	}
}

func (group GroupRouting) Register() {
	v1 := group.Engine.Group("/v1")
	{
		brandRoute := Brand.NewRoute(v1)
		brandRoute.Register()
	}
}